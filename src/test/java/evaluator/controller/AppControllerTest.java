package evaluator.controller;

import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.repository.IntrebariRepository;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by SESIN on 09-May-18.
 */
public class AppControllerTest {

    @Test
    public void TC1(){
        IntrebariRepository repo = new IntrebariRepository();
        AppController ctrl = new AppController();
        final String file1 = "C:\\Users\\SESIN\\Desktop\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\evaluator\\intrebariTestTC1.txt";

        try{
            ctrl.loadIntrebariFromFile(file1);
            ctrl.createNewTest();
            assert false;
        }
        catch (NotAbleToCreateTestException e) {
            assert true;
        }
    }

    @Test
    public void TC2(){
        IntrebariRepository repo = new IntrebariRepository();
        AppController ctrl = new AppController();
        final String file2 = "C:\\Users\\SESIN\\Desktop\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\evaluator\\intrebariTestTC2.txt";
        try{
            ctrl.loadIntrebariFromFile(file2);
            ctrl.createNewTest();
            assert true;
        }
        catch (NotAbleToCreateTestException e) {
            assert false;
        }
    }

    @Test
    public void TC3(){
        IntrebariRepository repo = new IntrebariRepository();
        AppController ctrl = new AppController();
        final String file3 = "C:\\Users\\SESIN\\Desktop\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\evaluator\\intrebariTestTC3.txt";
        try{
            ctrl.loadIntrebariFromFile(file3);
            ctrl.createNewTest();
            assert false;
        }
        catch (NotAbleToCreateTestException e) {
            assert true;
        }
    }

    @Test
    public void TC4(){
        IntrebariRepository repo = new IntrebariRepository();
        AppController ctrl = new AppController();
        final String file4 = "C:\\Users\\SESIN\\Desktop\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\evaluator\\intrebariTestTC4.txt";
        try{
            ctrl.loadIntrebariFromFile(file4);
            ctrl.getStatistica();
            assert false;
        }
        catch (NotAbleToCreateStatisticsException ex){
            assert true;
        }
    }

    @Test
    public void TC5(){
        IntrebariRepository repo = new IntrebariRepository();
        AppController ctrl = new AppController();
        final String file5 = "C:\\Users\\SESIN\\Desktop\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\evaluator\\intrebari.txt";
        try{
            ctrl.loadIntrebariFromFile(file5);
            ctrl.getStatistica();
            assert true;
        }
        catch (NotAbleToCreateStatisticsException ex){
            assert false;
        }
    }
}