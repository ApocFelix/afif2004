package evaluator.util;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by SESIN on 25-Apr-18.
 */
public class InputValidationTest {
    @Test
    public void TC1() throws Exception{
        InputValidation validator = new InputValidation();
        try{
            validator.validateEnunt("Sesin?");
            validator.validateVarianta1("1)Da");
            validator.validateVarianta2("2)NU");
            validator.validateVarianta3("3)Uneori");
            validator.validateVariantaCorecta("1");
            validator.validateDomeniu("Viata si moarte");
        }
        catch(Exception ex){
            assert false;
        }
        assert true ;
    }
    @Test
    public void TC2() throws Exception{
        InputValidation validator = new InputValidation();
        try{
            validator.validateEnunt("");
            validator.validateVarianta1("1)Da");
            validator.validateVarianta2("2)NU");
            validator.validateVarianta3("3)Uneori");
            validator.validateVariantaCorecta("1");
            validator.validateDomeniu("Viata si moarte");
        }
        catch(Exception ex){
            assert true;
            return;
        }
        assert false;
    }
}