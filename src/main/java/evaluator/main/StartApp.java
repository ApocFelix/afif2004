package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "C:\\Users\\SESIN\\Desktop\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\evaluator\\intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController();
		appController.loadIntrebariFromFile(file);
		
		boolean activ = true;
		String optiune;
		
		while(activ){

			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
			    String enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, domeniu;
			    System.out.println("Dati enuntul intrebarii: ");
			    enunt = console.readLine();
                System.out.println("Dati primul raspuns: ");
                raspuns1 = console.readLine();
                System.out.println("Dati al doilea raspuns: ");
                raspuns2 = console.readLine();
                System.out.println("Dati al treilea raspuns: ");
                raspuns3 = console.readLine();
                System.out.println("Dati raspunsul corect: ");
                raspunsCorect = console.readLine();
                System.out.println("Dati domeniul: ");
                domeniu = console.readLine();
                try{
                	Intrebare intrebare = new Intrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, domeniu);
					appController.addNewIntrebare(intrebare);
					break;
				}
				catch(InputValidationFailedException ex) {
					ex.printStackTrace();
				}
			case "2" :
			    try{
			    	appController.createNewTest();
				}
				catch(NotAbleToCreateTestException ex){
			    	ex.printStackTrace();
				}
				break;
			case "3" :
				//appController.loadIntrebariFromFile(file);
				Statistica statistica;
				try {
					statistica = appController.getStatistica();
					System.out.println(statistica);
				} catch (NotAbleToCreateStatisticsException e) {
					e.printStackTrace();
				}
				
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

}
